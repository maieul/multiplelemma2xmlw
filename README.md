# multiplelemma2xmlw

## Aim

- Take a CSV exported from Eulexis
- Merge all lemma of the same word in a list
- Output a xml containing `<w lemma="lemma1ofword1-lemma2ofword1">word1</w> <w lemma="lemma1ofword2-lemma2ofword1"></w>`

## Use

`python3 multiplelemma2xmlw <input.csv>`

## LICENCE

GPL 3
