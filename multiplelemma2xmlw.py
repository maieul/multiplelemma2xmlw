#!/usr/bin/env python3
# coding: utf-8

"""
Maïeul Rouquette - 2023
Version 1.0.2

This program is free software; you can redistribute it and/or modify
it under the terms of the GPL 3 license, available at
https://www.gnu.org/licenses/gpl.html
"""

import csv
import os.path
from sys import argv
class Multiplelemma2xmlw:
    """ Main class

    ------
    Attributes
    _input_path: string
        Path of the input file
    _data: list
        List of the words (object)

    """


    def __init__(self, input_path: str):
        """Init

        ----
        Parameters
        input_path : str
            Path to the file to import
        """

        self._input_path = input_path
        self._data = []
        self._import_data()

    def __str__(self):
        """Export a series of XML tags"""
        pre = """<?xml version="1.0"?>
            <!DOCTYPE TEI PUBLIC "TEI" "https://www.tei-c.org/release/xml/tei/custom/schema/dtd/tei_all.dtd">
            <TEI>
            <teiHeader>
            </teiHeader>
            <text>
            <body>"""
        post = """
		</body>
        </text>
        </TEI>"""


        return pre + "\n".join(map(str, self._data)) + post


    def _import_data(self):
        """Import data from CSV file to _data attributes
        """

        with open(self._input_path, encoding='utf-8') as csvfile:

            print("Importing data from" + self._input_path)
            reader = csv.reader(csvfile, delimiter="\t")
            first_line = True
            previous_NumW = -1
            for line in reader:
                if first_line == False:
                    if line[3] != '':
                        print ("*Import word number " + line[3])
                        current_numW = int(line[3]) - 1
                        current_form = line[4]
                        current_lemma = line[5]

                        if current_numW != previous_NumW:
                            self._data.append(Word(current_form))

                    self._data[current_numW].add_lemma(current_lemma)
                    previous_NumW = current_numW

                else:
                    first_line = False
            print ("End of importing")
    def export(self):
        """Export the file in XML format

        Filename is input filename + "-out" with an xml extension
        """
        input_fn, input_ex = os.path.splitext(self._input_path)
        output_fn = input_fn + "-out.xml"
        print ("Writing output file " + output_fn)
        output = open(output_fn, 'w', encoding='utf-8')
        output.write(str(self))
        output.close()
        print ("End of writing")


class Word:
    """ A class for a single word
    -----
    Attributes
    _form: str
    _lemmas: list
    """

    def __init__(self, form: str):
        """Instance the objet.

        ----
        Parameters
        form: str
            The (visible) form of the word
        ----
         """
        self._form = form
        self._lemma = []

    def __str__(self):
        '''Export as XML tag'''
        self._lemma.sort()

        return '<w lemma="' + '-'.join(self._lemma) + '">' + self._form + '</w>'

    def add_lemma(self, lemma: str):
       """Add a lemma to the list of the possible lemma of the word
       ----
       Parameters
       lemma: str
       """
       self._lemma.append(lemma.strip().replace(' ', '_').replace(',','_'))




run = Multiplelemma2xmlw(argv[1])
run.export()
